import React from 'react';
import {Link} from 'react-router';

const EditProfileSettings = props =>{
  if(props.isUser){
      return(
          <Link to="settings" className="btn btn-sm btn-outline-secondary action-btn white-btn">
              <i className="ion-gear-a"></i> Edit Profile Settings
          </Link>
      )
  }
  return null;
};

export default EditProfileSettings;