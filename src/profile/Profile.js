import ArticleList from '../home/ArticleList';
import React from 'react';
import {Link} from 'react-router';
import agent from '../agent';
import {connect} from 'react-redux';
import EditProfileSettings from './EditProfileSettings';
import FollowUserButton from './FollowUserButton';

const mapStateToProps = state => ({
    ...state.articleList,
    currentUser: state.common.currentUser,
    profile: state.profile
});

const mapDispatchToProps = dispatch => ({
    onFollow: username =>
        dispatch({type: 'FOLLOW_USER', payload: agent.Profile.follow(username)}),
    onUnfollow: username =>
        dispatch({type: 'UNFOLLOW_USER', payload: agent.Profile.unfollow(username)}),
    onLoad: payload =>
        dispatch({type: 'PROFILE_PAGE_LOADED', payload}),
    onUnload: () =>
        dispatch({type: 'PROFILE_PAGE_UNLOADED'}),
    onSetPage: (page,payload) => dispatch({type: 'SET_PAGE', page, payload})
});

class Profile extends React.Component {
    componentWillMount(){
        this.props.onLoad(Promise.all([
            agent.Profile.get(this.props.params.username),
            agent.Articles.byAuthor(this.props.params.username)
        ]));
    }
    componentWillUnmount(){
        this.props.onUnload();
    }
    renderTabs(){
        return(
            <ul className="nav nav-pills outline-active">
                <li className="nav-item">
                    <Link className="nav-link active" to={`@${this.props.profile.username}`}>
                        <span className="feed-color">My Articles</span>
                    </Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to={`@${this.props.profile.username}/favorites`}>
                        <span className="feed-color">Favorite Articles</span>
                    </Link>
                </li>
            </ul>
        )
    }
    onSetPage = (page) => {
        const promise = agent.Articles.byAuthor(this.props.profile.username, page);
        this.props.onSetPage(page, promise);
    };
    render(){
        const profile = this.props.profile;
        if(!profile){
            return null;
        }
        const isUser = this.props.currentUser && this.props.profile.username === this.props.currentUser.username;
        return(
            <div className="profile-page">
                <div className="user-info">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-md-10 offset-md-1">
                                <img src={profile.image} className="user-img"/>
                                <h4>{profile.username}</h4>
                                <p>{profile.bio}</p>
                                <EditProfileSettings isUser={isUser} />
                                <FollowUserButton isUser={isUser} user={profile} follow={this.props.onFollow}
                                                  unfollow={this.props.onUnfollow} />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-md-10 offset-md-1">
                            <div className="article-toggle">
                                {this.renderTabs()}
                            </div>
                            <ArticleList articles={this.props.articles} articlesCount={this.props.articlesCount}
                             currentPage={this.props.currentPage} onSetPage={this.onSetPage}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
export {Profile as Profile, mapStateToProps as mapStateToProps};