import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

// const API_ROOT = 'https://conduit.productionready.io/api';

const API_ROOT = 'http://localhost:3000/api';

const responseBody = res => res.body;
const limit = (count, p) => `limit=${count}&offset=${p ? p* count : 0}`;
const encode = encodeURIComponent;
const omitSlug = article => Object.assign({}, article, { slug: undefined });

let token = null;
const tokenPlugin = req => {
    if (token) {
        req.set('authorization', `Token ${token}`);
    }
};

const requests = {
    get: url =>
        superagent.get(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
    post: (url, body) =>
        superagent.post(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody),
    del: url =>
        superagent.del(`${API_ROOT}${url}`).use(tokenPlugin).then(responseBody),
    put: (url, body) =>
        superagent.put(`${API_ROOT}${url}`, body).use(tokenPlugin).then(responseBody)
};

const Articles = {
    all: page =>
        requests.get(`/articles?${limit(10,page)}`),
    get: slug =>
        requests.get(`/articles/${slug}`),
    recommendations: () =>
        requests.get('/articles/recommendations'),
    del: slug =>
        requests.del(`/articles/${slug}`),
    byAuthor: (author, page) =>
        requests.get(`/articles?author=${encodeURIComponent(author)}&${limit(10,page)}`),
    favoritesBy: (author, page) =>
        requests.get(`/articles?favorited=${encodeURIComponent(author)}&${limit(10,page)}`),
    feed: () =>
        requests.get('/articles/feed?limit=10&offset=0'),
    byTag: (tag, page) =>
        requests.get(`/articles?tag=${encodeURIComponent(tag)}&${limit(10,page)}`),
    create: article =>
        requests.post(`/articles`, {article}),
    update: article =>
        requests.put(`/articles/${article.slug}`, {article: omitSlug(article)}),
    favorite: slug =>
        requests.post(`/articles/${slug}/favorite`),
    unfavorite: slug =>
        requests.del(`/articles/${slug}/favorite`)
};

const Tags = {
    getAll: () => requests.get('/tags')
};

const Comments = {
    forArticle: slug =>
        requests.get(`/articles/${slug}/comments`),
    create: (slug, comment) =>
        requests.post(`/articles/${slug}/comments`, {comment}),
    del: (slug, commentId) =>
        requests.del(`/articles/${slug}/comments/${commentId}`),
    latest: () =>
        requests.get('/latest'),
};

const Auth = {
    current: () =>
        requests.get('/user'),
    login: (email, password) =>
        requests.post('/users/login', {user: {email, password}}),
    register: (username, email, password) =>
        requests.post('/users', {user: {username, email, password}}),
    save: user =>
        requests.put('/user', {user})
};

const Profile = {
    follow: username =>
        requests.post(`/profiles/${username}/follow`),
    unfollow: username =>
        requests.del(`/profiles/${username}/follow`),
    get: username =>
        requests.get(`/profiles/${username}`)
};

export default {
    Articles,
    Auth,
    Comments,
    Profile,
    Tags,
    setToken: _token => {
        token = _token;
    }
};