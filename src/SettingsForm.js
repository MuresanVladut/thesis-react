import React from "react";
import agent from "./agent";
import {connect} from "react-redux";

const mapStateToProps = state => ({
    ...state.settings,
    currentUser: state.common.currentUser
});

const mapDispatchToProps = dispatch => ({
    onSubmitForm: user =>
        dispatch({type: 'SETTINGS_SAVED', payload: agent.Auth.save(user)})
});

class SettingsForm extends React.Component {
    constructor() {
        super();

        this.state = {
            image: '',
            username: '',
            bio: '',
            email: '',
            password: ''
        };

        this.updateState = field => ev => {
            const state = this.state;
            const newState = Object.assign({}, state, {[field]: ev.target.value});
            this.setState(newState);
        };

        this.submitForm = ev => {
            ev.preventDefault();

            const user = Object.assign({}, this.state);
            if (!user.password) {
                delete user.password;
            }

            this.props.onSubmitForm(user);
        }
    }

    componentWillMount() {
        this.updateStateAtLifecycle(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateStateAtLifecycle(nextProps);
    }

    updateStateAtLifecycle(props) {
        if (props.currentUser) {
            Object.assign(this.state, {
                image: props.currentUser.image || '',
                username: props.currentUser.username,
                bio: props.currentUser.bio,
                email: props.currentUser.email
            });
        }
    }

    render() {
        return (
            <form onSubmit={this.submitForm}>
                <fieldset>
                    <fieldset classID="form-group">
                        <input
                            className="form-control"
                            type="text"
                            placeholder="URL of profile picture"
                            value={this.state.image}
                            onChange={this.updateState('image')}/>
                    </fieldset>

                    <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="text"
                            placeholder="Username"
                            value={this.state.username}
                            onChange={this.updateState('username')}/>
                    </fieldset>
                    <fieldset className="form-group">
                        <textarea
                            className="form-control form-control-lg"
                            rows="8"
                            placeholder="Short bio about you"
                            value={this.state.bio}
                            onChange={this.updateState('bio')}>
                        </textarea>
                    </fieldset>
                    <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="email"
                            placeholder="Email"
                            value={this.state.email}
                            onChange={this.updateState('email')}/>
                    </fieldset>
                    <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="password"
                            placeholder="Old Password"
                            value={this.state.old_password}
                            onChange={this.updateState('old_password')}/>
                    </fieldset>
                    <fieldset className="form-group">
                        <input
                            className="form-control form-control-lg"
                            type="password"
                            placeholder="New Password"
                            value={this.state.password}
                            onChange={this.updateState('password')}/>
                    </fieldset>
                    <button
                        className="btn btn-lg btn-primary pull-xs-right"
                        type="submit"
                        disabled={this.state.inProgress}>
                        Update Settings
                    </button>
                </fieldset>
            </form>
        );
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SettingsForm);