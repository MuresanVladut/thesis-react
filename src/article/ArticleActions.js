import { Link } from 'react-router';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
    onClickDelete: (payload) => dispatch({type: 'DELETE_ARTICLE', payload}),
    onToggleFollowing: (payload) => dispatch({type: 'TOGGLE_FOLLOW', payload }),
    onToggleFavorite: (payload) => dispatch({type: 'TOGGLE_FAVORITE', payload}),
});

const ArticleActions = props => {
    const article = props.article;
    const del = () =>{
        props.onClickDelete(agent.Articles.del(article.slug))
    };
    const toggleFollowing = () => {
        if(article.author.following){
            props.onToggleFollowing(agent.Profile.unfollow(article.author.username));
        } else {
            props.onToggleFollowing(agent.Profile.follow(article.author.username));
        }
    };
    const toggleFavorite = () => {
        if(!article.favorited){
            props.onToggleFavorite(agent.Articles.favorite(article.slug));
        } else {
            props.onToggleFavorite(agent.Articles.unfavorite(article.slug));
        }
    };
    if(props.canModify){
      return(
          <span>
              <Link to={`/editor/${article.slug}`} className="btn btn-outline-secondary btn-sm white-btn">
                  <i className="ion-edit"/> Edit Article
              </Link>

              <button className="btn btn-outline-danger btn-sm white-btn" onClick={del}>
                  <i className="ion-trash-a"/> Delete Article
              </button>
          </span>
      )
    }
    return(
        <span>
            <button className={!article.author.following ? 'btn btn-sm action-btn btn-outline-secondary'
                : 'btn btn-sm action-btn btn-secondary'} onClick={toggleFollowing}>
             <i className="ion-plus-round"/>&nbsp;{article.author.following? 'Unfollow': 'Follow'} {article.author.username}
            </button>
            <button className={!article.favorited ? 'btn btn-sm btn-outline-primary white-btn' : 'btn btn-sm btn-primary'}
                    onClick={toggleFavorite}>
                <i className="ion-heart"/>
                {article.favorited ? 'Unfavorite' : 'Favorite' } Article
                <span className="counter"> ({article.favoritesCount })</span>
            </button>
        </span>
    );
};

export default connect(()=>({}), mapDispatchToProps)(ArticleActions)