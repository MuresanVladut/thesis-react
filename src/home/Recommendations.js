import React from 'react';
import {Link} from 'react-router';


const Recommendations = props => {
    const articles = props.articles;
    if(articles){
        return(
            <div>
                <p>Articles Suggestion</p>
                <div className="tag-list">
                    {articles.map((article,index)=>{
                        return (
                            <div key={index}>
                                <Link to={`article/${article.slug}`} className="tag-default tag-pill"
                                      data-tip data-for={`${index}`}>
                                    {article.title}
                                </Link>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    } else {
        return null;
    }
};

export default Recommendations;

