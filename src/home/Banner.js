import React from 'react';

const Banner = ({appName}) => {
  return (
      <div className="banner">
          <div className="container">
              <h1 className="logo-front">
                  {appName}
              </h1>
              <p>A place to share your ReactJS knowledge</p>
          </div>
      </div>
  )
};

export default Banner;