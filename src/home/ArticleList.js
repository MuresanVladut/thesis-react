import React from 'react';
import ArticlePreview from './ArticlePreview';
import ListPagination from './../ListPagination';

const ArticleList = (props) =>{
    if(!props.articles){
        return(
            <div className="article-preview">Loading...</div>
        )
    }

    if(props.articles.length === 0){
        return(
            <div className="article-preview">No articles are here...yet</div>
        )
    }
    return(
        <div>
            {props.articles.map((article, index)=>{
                return (
                   <ArticlePreview key={index} article={article}/>
                )
            })}

            <ListPagination articlesCount={props.articlesCount} currentPage={props.currentPage}
                onSetPage={props.onSetPage}/>
        </div>
    )
};

export default ArticleList;