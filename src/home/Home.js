import MainView from './MainView';
import React from 'react';
import {connect} from 'react-redux';
import agent from "../agent";
import Tags from './Tags';
import LatestComments from "./LatestComments";
import Recommendations from "./Recommendations";

const Promise = global.Promise;

const mapStateToProps = state => ({
    ...state.home,
    ...state.common,
    appName: state.common.appName,
    token: state.common.token
});

const mapDispatchToProps = dispatch => ({
    onLoad: (tab, payload) => dispatch({type: 'HOME_PAGE_LOADED', tab, payload}),
    onLoadWithRecommendations: (tab, payload) => dispatch({type: 'HOME_PAGE_LOADED_WITH_RECOMMENDATIONS', tab, payload}),
    onUnload: () => dispatch({type: 'HOME_PAGE_UNLOAD'}),
    onClickTag: (tag, payload) => dispatch({type: 'APPLY_TAG_FILTER', tag, payload})
});

class Home extends React.Component {
    componentWillMount() {
        const tab = this.props.token ? 'feed' : 'all';
        const articlesPromise = this.props.token ?
            agent.Articles.feed() :
            agent.Articles.all();

        if(this.props.currentUser){
            this.props.onLoadWithRecommendations(tab, Promise.all([agent.Tags.getAll(),agent.Comments.latest(),articlesPromise,agent.Articles.recommendations()]));
        } else {
            this.props.onLoad(tab, Promise.all([agent.Tags.getAll(),agent.Comments.latest(),articlesPromise]));
        }
    }

    componentWillUnmount(){
        this.props.onUnload();
    }

    render() {
        return (
            <div className="home-page">
                <div className="container page">
                    <div className="row">
                        <MainView/>
                        <div className="col-md-3">
                            <div className="sidebar">
                                <Recommendations articles={this.props.recommendations}/>
                                <p>Latest Comments</p>
                                <LatestComments comments={this.props.comments}/>
                                <p>Popular Tags</p>
                                <Tags tags={this.props.tags} onClickTag={this.props.onClickTag}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);