import React from 'react';
import {Link} from 'react-router';
import agent from './../agent';
import {connect} from 'react-redux';

const FAVORITED_CLASS = 'btn btn-sm btn-primary';
const NOT_FAVORITED_CLASS = 'btn btn-sm btn-outline-primary';

const mapDispatchToProps = dispatch => ({
    onFavorite: (author,slug) =>{
        dispatch({type: 'FAVORITE', payload: agent.Articles.favorite(slug)});
    },
    onUnfavorite: (author,slug) => {
        dispatch({type: 'UNFAVORITE', payload: agent.Articles.unfavorite(slug)});
    }
});

const ArticlePreview = props => {
    const article = props.article;
    const handleClick = ev =>{
        ev.preventDefault();
        if(article.favorited){
            props.onUnfavorite(article.author.username, article.slug);
        } else {
            props.onFavorite(article.author.username, article.slug)
        }
    };
    return(
        <div className="article-preview">
            <div className="article-meta">
                <Link to={`@${article.author.username}`}>
                    <img src={article.author.image}/>
                </Link>
                <div className="info">
                    <Link to={`@${article.author.username}`} className="author">
                        {article.author.username}
                    </Link>
                    <span className="date">
                        {new Date(article.createdAt).toDateString()}
                    </span>
                </div>
                <div className="pull-xs-right">
                    <button className={article.favorited ? FAVORITED_CLASS : NOT_FAVORITED_CLASS} onClick={handleClick}>
                        <i className="ion-heart"/> {article.favoritesCount}
                    </button>
                </div>
            </div>
            <Link to={`article/${article.slug}`} className="preview-link">
                <h1>{article.title}</h1>
                <p>{article.description}</p>
                <span>Read more...</span>
                <ul className="tag-list">
                    {
                        article.tagList.map(tag => {
                            return(
                                <li className="tag-default tag-pill tag-outline" key={tag}>{tag}</li>
                            )
                        })
                    }
                </ul>
            </Link>
        </div>
    )
};

export default connect(()=>({}), mapDispatchToProps)(ArticlePreview);