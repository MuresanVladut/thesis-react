import React from 'react';
import {Link} from 'react-router';


const LatestComments = props => {
    const comments = props.comments;
    if(comments){
        return(
            <div className="tag-list">
                {comments.map((comment,index)=>{
                    return (
                        <div key={index}>
                            <Link to={`article/${comment.article.slug}`} className="tag-default tag-pill"
                                data-tip data-for={`${index}`}>
                                {`${comment.author.username} posted on article: ${comment.article.title}`}
                            </Link>
                        </div>
                    )
                })}
            </div>
        )
    } else {
        return (
            <div>Loading Comments...</div>
        )
    }
};

export default LatestComments;

