import {Provider} from 'react-redux';
import ReactDOM from 'react-dom';
import React from 'react';
import App from './App';
import store from './store';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import Home from "./home/Home";
import Login from "./login/Login";
import Register from "./Register";
import Settings from "./Settings";
import Article from './article/index';
import Profile from './profile/Profile'
import ProfileFavorites from "./profile/ProfileFavorites";
import Editor from "./Editor";

ReactDOM.render((
    <Provider store={store}>
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home} activeClassName="active"/>
            <Route path="login" component={Login}/>
            <Route path="register" component={Register}/>
            <Route path="settings" component={Settings}/>
            <Route path="article/:id" component={Article}/>
            <Route path="@:username" component={Profile}/>
            <Route path="@:username/favorites" component={ProfileFavorites}/>
            <Route path="editor" component={Editor} />
            <Route path="editor/:slug" component={Editor} />
        </Route>
    </Router>
    </Provider>
), document.getElementById('root'));