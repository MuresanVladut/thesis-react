export default (state = {}, action) => {
    switch (action.type) {
        case 'ARTICLE_PAGE_LOADED':
            return {
                ...state,
                article: action.payload[0].article,
                comments: action.payload[1].comments
            };
        case 'ARTICLE_PAGE_UNLOADED':
            return {};
        case 'ADD_COMMENT':
            return{
                ...state,
                commentErrors: action.error ? action.payload.errors : null,
                comments: action.error ? null : ([action.payload.comment]).concat(state.comments || [])
            };
        case 'DELETE_COMMENT':
            const commentId = action.commentId;
            return{
                ...state,
                comments: state.comments.filter(comment=>comment.id !== commentId)
            };
        case 'TOGGLE_FOLLOW':
            return {
                ...state,
                article: {
                    ...state.article,
                    author: {
                        ...state.article.author,
                        following: action.payload.profile.following
                    }
                }
            };
        case 'TOGGLE_FAVORITE':
            return {
                ...state,
                article: {
                    ...state.article,
                    favorited: action.payload.article.favorited,
                    favoritesCount: action.payload.article.favoritesCount
                    }
                }
            }

    return state;
};