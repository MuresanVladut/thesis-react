export default (state={}, action) => {
    switch (action.type){
        case 'HOME_PAGE_LOADED':
            return {
                ...state,
                tags: action.payload[0].tags,
                comments: action.payload[1].comments,
                articles: action.payload[2].articles,
            };
        case 'HOME_PAGE_LOADED_WITH_RECOMMENDATIONS':
            return {
                ...state,
                tags: action.payload[0].tags,
                comments: action.payload[1].comments,
                articles: action.payload[2].articles,
                recommendations: action.payload[3].articles,

            };
        case 'HOME_PAGE_UNLOADED':
            return {};
    }
    return state;
}

